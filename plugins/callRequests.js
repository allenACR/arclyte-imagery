
//  WHEN TESTING LOCALLY

//


// WHEN PUBLISHED LIVE
// var myHost = 'http://frserver.gravityjack.com'







//////////////////////// 
//  CREATE NEW ACCOUNT
//////////////////////// 
function joinSubmit(myBtn) {
	 
	setTimeout(function () {
		returnFromJoin()        
	}, 1000);		

}

//////////////////////// 
//  SUBMIT TO QUERY
//////////////////////// 
function submitToLogin( iUsername, iPassword ) {
	
	userData.set("userName", 	"userName")
	userData.set("accountType", "admin")

    setTimeout(function () {
        returnFromLogin()        
    }, 1000);		

}


//////////////////////// 
//  LOST PASSWORD / RETRIEVE
//////////////////////// 
function submitToRetrieve(){
    setTimeout(function () {
        returnFromRetireve()        
    }, 1000);	
}


//////////////////////// 
//  CREATE NEW USER
//////////////////////// 
function submitCreateUser(){
    setTimeout(function () {
        returnFromCreateUser()        
    }, 1000);	
}

//////////////////////// 
//  GET MORE DETAILS
//////////////////////// 
function submitRetrieve(email) {

    showAsyncThinking();
    setTimeout(function () {
        returnFromRetrieve()        
    }, 1000);		

}

//////////////////////// 
//  FETCH APPROVAL
//////////////////////// 
function fetchApproval(){
    
	
	
	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/ApprovalRequest/',
				success: function(data){
						rootStorage.set("approvalResults", JSON.stringify(data) )     
						//var data = JSON.parse(rootStorage.get("approvalResults")).results  //BREAK APART				
						returnFromApproval(true, data)	
				},
				error: function(data){		 
						returnFromApproval(false)
				}                       
	});	
	


}

//////////////////////// 
//  SUBMIT TO ADD IMAGE
//////////////////////// 
function submitToAddImage(){

	setTimeout(function () {
        returnFromAddImage()        
    }, 1000);	
}

//////////////////////// 
//  SUBMIT FOR APPROVAL
///////////////////////
function submitToApproval(){

	setTimeout(function () {
        returnFromSubmitApproval()        
    }, 1000);

}

//////////////////////// 
//  SUBMIT GET IMAGE DATA
///////////////////////
function submitGetImageData(){


	setTimeout(function () {
        returnFromGetImageData()        
    }, 1000);
	
}


//////////////////////// 
//  UPDATE USER PROFILE
///////////////////////
function updateUserProfile(){

	setTimeout(function () {
        returnFromUpdateProfile()        
    }, 1000);
	
}


//////////////////////// 
//  SUBMIT TO QUERY
///////////////////////
function submitToQuery( images, type, size, x1, y1, w, h ) {

				var queryImage = new FormData();
					queryImage.append('image', images.files[0]);
					if (x1 != ""){ 
					queryImage.append('x', x1)
					}
					if (y1 != ""){ 
					queryImage.append('y', y1)
					}
					if (w != ""){ 
					queryImage.append('width', w)
					}
					if (h != ""){ 
					queryImage.append('height', h)
					}					
				
				
				
				if (type == "RES"){
				
					queryImage.append('resolution', size);
				
					$.ajax({	 
						type: 'POST',  
						url: myHost + '/api/v0/images/',
						data: queryImage,
						cache: false,
						contentType: false,
						processData: false,
						success: function(data){			
								submitToQueryPart2( data.results._id )
						},
						error: function(data){
							
								returnFromSubmit(false, data)     
						}                       
					});
				}

				else{
				
					queryImage.append('widthCm', size);
				
					$.ajax({	 
						type: 'POST',
						url: myHost + '/api/v0/images/',
						data: queryImage,
						cache: false,
						contentType: false,
						processData: false,
						success: function(data){									
								submitToQueryPart2( data.results._id )	
						},
						error: function(data){
								returnFromSubmit(false, data)
								
						}                       
					});
				}				
		
			

			
}

function submitToQueryPart2( queryID ){

				var bodyData = new FormData();
					bodyData.append('id', queryID);

					$.ajax({	 
						type: 'POST',  
						url: myHost + '/api/v0/IEDExample/query',
						data: bodyData,
						cache: false,
						contentType: false,
						processData: false,
						success: function(data){	
							addToQueryResults(data)
							returnFromSubmit(true, data)
						},
						error: function(data){
							
							returnFromSubmit(false) 
						}                       
					});

}




//////////////////////// 
//  SUBMIT TO QUERY
///////////////////////
function addToQuery( images, widthCm ) {
		
           
			if ( $(images).val() == undefined || $(images).val() == null || $(images).val() == ""){
				
			}
			else{
				showThinking();
				
				var queryImage = new FormData();
				jQuery.each(	$(images)[0].files, function(i, file) {
                        
						queryImage.append('image', file);
				});
					
				$.ajax({ 
							type: 'POST',
							url: myHost + '/api/v0/add?widthCm=' + widthCm,
							data: queryImage,
							dataType: 'applicaton/JSON',
							cache: false,
							contentType: false,
							processData: false,
							success: function(data){
									updateConsole("Added image to database.");      
									returnFromAddImage(true, data)
										
									
							},
							error: function( xhr, status, errorThrown){
								
									updateConsole("Internal server error.");       
                                    returnFromAddImage(false, status)
									
							}                       
				});
			}
}

//////////////////////// 
//  SUBMIT TO QUERY
///////////////////////
function submitAdditionalPages( queryID, pageNum ) {
		
		
	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/query?id=' + queryID + "&page=" + pageNum,
				success: function(data){
						returnFromAddPages(true, data)			
				},
				error: function(data){		 
						returnFromAddPages(false, data)	   
				}                       
	});	
	
}







//////////////////////// 
//  GET MORE DETAILS
//////////////////////// 
function submitMoreDetails(searchFor) {


	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IEDExample/?id=' + searchFor,
				success: function(data){
						getIEDDetails( data );
						updateConsole("Retrieval is successful!");      
						//returnFromDetails(true, data)
							
						
				},
				error: function(data){
						
						returnFromDetails(false, data)
						updateConsole("Internal server error.");       
				}                       
	});	

}



//////////////////////// 
//  GET MORE DETAILS
//////////////////////// 
function getIEDDetails(dataSet1) {
	
	
	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IED/?id=' + dataSet1[0].exampleOfIED,
				success: function(data){
						getIEDProfile(dataSet1, data);
						updateConsole("Retrieval is successful!");      
						//returnFromDetails(true, data)
							
						
				},
				error: function(data){
						returnFromDetails(false, data)
						updateConsole("Internal server error.");       
				}                       
	});	

}


//////////////////////// 
//  GET MORE DETAILS
//////////////////////// 
function getIEDProfile(dataSet1, dataSet2) {
	

	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IEDProfile/?ied_id=' + dataSet1[0].exampleOfIED + '&id=' + dataSet1[0].exampleOfProfile,
				success: function(data){						
						   
						returnFromDetails(true, dataSet1, dataSet2, data)
							
						
				},
				error: function(data){
						returnFromDetails(false, data)
					
				}                       
	});	

}



////////// CLASS PAGE 
//
function submitForClasses() {

	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IED/',
				success: function(data){	
                        rootStorage.set("queryResults_classes", JSON.stringify(data) )  
						returnFromClasses(true)		
				},
				error: function(data){
						returnFromClasses(false)
				}                       
	});	
		
}

function submitForClassProfiles( id ){

	var data = JSON.parse(rootStorage.get("queryResults_classes")).results
						
	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IEDExample/?ied_id=' + data[id]._id,
				success: function(data){			
						rootStorage.set("queryResults_allClass", JSON.stringify(data) )     
						//var data = JSON.parse(rootStorage.get("queryResults_allClass")).results  //BREAK APART
						getProfiles()
		
				},
				error: function(data){
						returnFromClassData(false)
					
				}                       
	});	


	function getProfiles(){
	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IEDProfile/?ied_id=' + data[id]._id,
				success: function(data){						
						rootStorage.set("queryResults_classProfile", JSON.stringify(data) )     
						//var data = JSON.parse(rootStorage.get("queryResults_classProfile")).results  //BREAK APART
						returnFromClassData(true, id)
							
						
				},
				error: function(data){
						returnFromClassData(false)
					
				}                       
	});	
	}

}
//
//////////////



function submitForUsers() {

	setTimeout( function(){
		returnFromUsers()
	}, 1000); 
			
}

function fetchClassesSpecifics( id , profile_id){


	var data = JSON.parse(rootStorage.get("queryResults_classes")).results
	
	$.ajax({                      
				type: 'GET',
				url: myHost + '/api/v0/IEDExample/?ied_id=' + data[id]._id + '&profileId=' + profile_id,
				success: function(data){						
						returnClassesSpecifics(true, data)		
				},
				error: function(data){
						returnClassesSpecifics(false)
					
				}                       
	});	
	

}


function submitForBrowse( isCommercial , isReciever, isTransmitter, iedID ) {

	var urlString = '/api/v0/IEDExample/?'	
	if (isCommercial){
		urlString += 'isCommercial=true&'
	}
	if (isReciever){
		urlString += 'isReciever=true&'
	}	
	if (isTransmitter){
		urlString += 'isTransmitter=true&'
	}
	if (iedID != ""){
		urlString += 'profileId=' + iedID
	}
	

	$.ajax({                      
				type: 'GET',
				url: myHost + urlString,
				success: function(data){						
						rootStorage.set("browseResults", JSON.stringify(data) )     
						//var data = JSON.parse(rootStorage.get("browseResults")).results 	 //BREAK APART
						returnFromBrowse(true)
							
						
				},
				error: function(data){
						returnFromBrowse(false)					
				}                       
	});	

	

			
}

//////////////////////// 
//  CHECK FOR USERNAME
//////////////////////// 
function checkForUsername( $myDiv ){
	
    showAsyncThinking();
    setTimeout(function () {
        returnFromCheckUsername( true, $myDiv)        
    }, 1000);		

}	


//////////////////////// 
//  GET MORE DETAILS
//////////////////////// 
function checkEmailAvail( $myDiv ){
	
    showAsyncThinking();
    setTimeout(function () {
        returnFromCheckEmail( true, $myDiv)        
    }, 1000);		

}	


