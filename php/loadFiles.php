<?php
  echo '
		<!-- jquery -->
		<script src="jQuery/jquery-1.10.2.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

		<!-- plugins -->
    	<script src="plugins/bootstrap.js"></script>
        <script src="plugins/resizeStop.js"></script>
        <script src="plugins/ytPlugin.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>


		<!-- breezejs -->
		<script src="plugins/q.min.js"></script>
		<script src="plugins/breeze.min.js"></script>

        <!-- custom scripts -->
		<script src="modScript/setup.js"></script>
		<script src="modScript/addEffects.js"></script>
        <script src="modScript/localStorage.js"></script>
  ';

?>