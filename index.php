<!DOCTYPE html>
<html>
  <head>
		<title>Arclyte Imagery</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
			<!-- FAVICON -->
				<link rel="icon" type="image/jpg" href="siteImg/favIcon.jpg">
	
	
			<!-- REQUIRED CSS SCRIPTS / MUST BE INCLUDED BEFORE BODY -->
				<link type="text/css" href="plugins/bootstrap/css/bootstrap.css"				rel="stylesheet" media="screen" />   	<!-- required -->
				<link type="text/css" href="plugins/bsValidator/css/bootstrapValidator.css" 	rel="stylesheet" />						<!-- required -->
				<link type="text/css" href="plugins/modal/css/bootstrap-modal.css"				rel="stylesheet" />						<!-- required -->
				<link type="text/css" href="plugins/modal/css/bootstrap-modal-bs3patch.css"		rel="stylesheet" />						<!-- required -->
				<link type="text/css" href="plugins/colorBox/css/colorbox.css"					rel="stylesheet" />						<!-- required -->
				<link type="text/css" href="plugins/pageTransistions/css/multilevelmenu.css" 	rel="stylesheet" />						<!-- required -->
				<link type="text/css" href="plugins/pageTransistions/css/component.css" 		rel="stylesheet" />						<!-- required -->
				<link type="text/css" href="plugins/pageTransistions/css/animations.css"		rel="stylesheet" />						<!-- required -->

				
				<!-- PLUGINS -->							
				<link type="text/css" href="plugins/creativeLinks/css/component.css" 			rel="stylesheet"/>		<!-- optional animatedLinks -->					
				<link type="text/css" href="plugins/custom/css/stylesheet.css"					rel="stylesheet"/>		<!-- required CUSTOM included at end of CSS-->				
			<!----->
			
			
			<!-- REQUIRED JS SCRIPTS / MUST BE INCLUDED BEFORE BODY -->
				<script src="plugins/pageTransistions/js/modernizr.custom.js"></script>									<!-- required for several plugins -->
				<script src="plugins/angular/js/angular1.0.8.js"></script>												<!-- required -->
				<script src="plugins/persistJS/js/persist-all-min.js"></script>											<!-- required -->
				<script src="plugins/callRequests.js"></script>															<!-- required -->
			<!---->	 
			
			<!-- FONTS -->
				<link href='http://fonts.googleapis.com/css?family=Belgrano' rel='stylesheet' type='text/css'>
			<!---->	
				 
		
			<style>
				#pt-main{
					top: 0px; 
				}			
			</style>
	
  </head>
  <body id="" class="" >
	
		<!-- LOADING SCREEN -->
		<div id="loadingScreen" class="" style="width: 100%; height: 100%; position: fixed">
				<img src="siteImg/background.jpg" class=""/>
		</div>
		<!---->
		
		<div id="wrapper">
				<!-- PERSIST THROUGH TRANSISTIONS -->
				<div id="persistLayer" >
				</div>	
				<!-- END -->
		
				<!-- BACKGROUND LAYER -->				
				<div id="bgLayer" class="" style="width: 100%; height: 100%; position: fixed">
						<img src="siteImg/background.jpg" class=""/>
				</div>				
				<!-->
		
				<!-- CONTENT GOES HERE -->
				<div id="pt-main" class="pt-perspective">
		
				
					<div id="contentPage1" class="pt-page pt-page-1 " >					
					</div>
					<div id="contentPage2" class="pt-page pt-page-2 ">	
					</div>
		
							
					
				</div>
				<!-- END -->	
		</div>
	

	
		<div id="phpCapture">
			
		</div>


	
  </body>
  
  
			<!-- LOAD PLUGINS -->
				<script src="settings/settings.js"></script>										<!-- required -->
				<script src="plugins/jQuery/jquery-min.js"></script>								<!-- required -->
				<script src="plugins/jQuery/jquery-ui-1.10.3.custom.js"></script>					<!-- required -->
				<script src="plugins/bootstrap/js/bootstrap.min.js"></script>						<!-- required -->	
				<script src="plugins/growl/js/jquery.bootstrap-growl.js"></script>				<!-- required growl -->				
				<script src="plugins/colorBox/js/jquery.colorbox.js"></script>						<!-- required colorBox -->
				<script src="plugins/modal/js/bootstrap-modal.js"></script>							<!-- required for advanced modals -->
				<script src="plugins/modal/js/bootstrap-modalmanager.js"></script>					<!-- required for advanced modals -->
				<script src="plugins/bsValidator/js/bootstrapValidator.js"></script>				<!-- required for form validation -->
				<script src="plugins/pageTransistions/js/jquery.dlmenu.js"></script>				<!-- required for transitions -->
				<script src="plugins/pageTransistions/js/pagetransitions.js"></script>				<!-- required for transitions -->
				<script src="plugins/custom/js/global.js"></script>									<!-- required -->				
			<!-- LOAD PLUGINS -->
		
	
	
	<script> 	

	
	
	////////////////////////
	//  LOAD INITIAL 
	function refreshInit(){
	
		$('#persistLayer').empty();
		
	
		/////////////////////////////////////////////////////////////
		//  LOAD ORDER	
		

		  loadOrder = [	
				//['templateExamples/navigation/animatedBorders.html',		'#persistLayer'],
				//['templateExamples/bootstrap/bootstrapNav.html', 			'#persistLayer'],
				['views/loadingScreens.html', 	'#persistLayer'],
				['templateExamples/pageSlide/pageSlideDemo.html', 			'#persistLayer'],					
			];			

			

			processLoadOrder(loadOrder, function(returnState){
				
				if (returnState){
					
					clearAllSections();					
					sub_Loader.show();
					
					fetchAllData( function(){
						sub_Loader.hide();
						setTimeout( function(){	
							if ( getUrlValue() == ""){

								changeHash('home');
							} 
							else { 											
								changeScreens( getUrlValue()  ); 		
							} 	
						}, 200); 						
							
					});					
					

						
				}
				else{
					alert("Page could not load AJAX files.  If you are using Chrome, disable security mode or use Firefox instead.");
					
				}
				
			});
			
	}
	//
	////////////////////////
	

	////////////////////////
	// CHANGE SCENES
	var currentScreen 	= 1;
	var priorScreen		= 0;
	var pageTrans		= 22; // DEFAULT PAGE TRANSISTION TYPE
	var firstLoad		= false
	function changeScreens(type, transType){
	

		// TRANSISTION BEHAVIOR
		//
		transType  = 21;
		if (transType == undefined || transType == null){ transType = 43; } ;
			priorScreen = currentScreen;
			currentScreen ++;		
		if (currentScreen > 2){ currentScreen = 1; }; 
			nextPageTrans(transType);
			
			var thePage = '#contentPage' + currentScreen;
		//
		///////////////////////
		
	
		
		////////////////////  
		//
		var loadOrder
				loadOrder = [	
					['views/' + type + '.html', 	thePage]
				];	
		//
		////////////////////  
		
		

		processLoadOrder(loadOrder, function(returnState){
			
			
			if (returnState){
				if (!firstLoad){ firstLoad = true; loadCompleted() }
			}
			else{
				alert("Page could not load AJAX files.  If you are using Chrome, disable security mode or use Firefox instead.");
				
			}
			
		});		
			
		


	}
	//
	////////////////////////
		

	////////////////////////
	//
    function callPHPFunction(type, callback ){
		 trace(type);
		
         $.ajax({ url: 'php/phpFunctions.php',
                 data: {action: type},
                 type: 'post',
                 success: function(data) {
					trace("PHP call success.")             					
					callback(true, data)	
                 },
				 error: function(data){
					trace("PHP call error.")
					callback(false, data)   
				 }
        });

    }	
	//
	////////////////////////

	
	
	///// FETCH ALL IMAGE DATA
	//
	function fetchAllData( callback ){
		var i = 1; 
		var max = 5; 
		
		function start(){
			
			if(!loadVar("getGallery" + i)) {
				callPHPFunction('getGallery' + i, function(returnState, data){							
					if (returnState){												
						saveVar("getGallery" + i, (data) )
						next();	
					}
					else{
						next();	
					}
						
				});					
			}
			// GALLERY ALREADY EXIST; CONTINUE
			else{
				next();
			};	
		};
		
		function next(){
			
			i++; 
			if (i >= max){ 	end(); 		}
			else{			start();	}
		}
		
		function end(){
			callback()
		}
		
		start();
	
	}
	/////  END	

	/////  LOAD COMPLETE
	//
	//deleteVar('getGallery0')
	//deleteVar('getGallery1')
	//deleteVar('getGallery2')
	//deleteVar('getGallery3')
	//deleteVar('getGallery4')
			
	function loadCompleted(){

				
			$('#wrapper').fadeIn( 500, function(){		
				$('#wrapper').css( 'overflow-y', 'auto' );	
				$('#loadingScreen').empty(); 					// REMOVE LOADING SCREEN IMAGE	
			});
			//changeBGLayer('colorSchemeC', 3000)				
			//subLoader.show()

	}
	

	
			
	/////  INIT
	//
	
		$('#wrapper').fadeOut( 0 );						 
		$('#wrapper').css( 'overflow', 'hidden' );		
		initCheck();
		refreshInit(); 
	
	//
	/////
	

	
	

	

	
	
	

	</script>
	
  
  
  
  
</html>